<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCleanerLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->down();
        Schema::create('cleaner_locations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->decimal('latitude',10,8);
            $table->decimal('longitude',11,8);
            $table->unsignedBigInteger('cleaner_id');
            $table->foreign('cleaner_id')->references('id')->on('cleaners');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cleaner_locations');
    }
}

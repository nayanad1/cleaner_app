<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->down();
        Schema::create('user_requests', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->decimal('latitude',10,8);
            $table->decimal('longitude',11,8);
            $table->tinyInteger('status')->default('0');
            $table->unsignedBigInteger('app_user_id');
            $table->foreign('app_user_id')->references('id')->on('app_users');
            $table->unsignedBigInteger('cleaner_id')->nullable();
            $table->foreign('cleaner_id')->references('id')->on('cleaners');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_requests');
    }
}

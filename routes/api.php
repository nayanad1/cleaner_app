<?php

use App\Http\Controllers\AppUserController;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//Route::post('user-register',function (Request $request){
//    return AppUserController::create($request->all());
//});

//user
Route::post('user-register', 'AppUserController@store');
Route::post('user-login', 'AppUserController@login');
Route::post('user-logout', 'AppUserController@logout');
Route::post('cleaner-request', 'AppUserController@requestCleaner');


//cleaner
Route::post('cleaner-register', 'CleanerController@store');
Route::post('cleaner-login', 'CleanerController@login');
Route::post('cleaner-logout', 'CleanerController@logout');
Route::post('complete-request', 'CleanerController@completeJob');


Route::get('start-firebase','FirebaseController@createInstance');

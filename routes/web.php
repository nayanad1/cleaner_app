<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//admin
Route::get('login', 'AdminController@index');
Route::post('admin-home', 'AdminController@login')->name('admin-home');
Route::get('logout','AdminController@logout')->name('logout');
Route::get('add-admin','AdminController@addAdmin')->name('add-admin');
Route::post('save-admin','AdminController@store')->name('save-admin');

//user
Route::get('home', 'AdminController@home')->name('home');
Route::get('view-user/{id}', 'AppUserController@viewUser')->name('view-user');

//cleaner
Route::get('view-cleaners', 'CleanerController@viewCleaners')->name('view-cleaners');
Route::get('view-cleaner/{id}', 'CleanerController@viewCleaner')->name('view-cleaner');
Route::get('add-cleaner', 'CleanerController@addCleaner')->name('add-cleaner');
Route::post('save-cleaner', 'CleanerController@saveCleaner')->name('save-cleaner');
Route::get('delete-cleaner/{id}', 'CleanerController@destroy')->name('delete-cleaner');

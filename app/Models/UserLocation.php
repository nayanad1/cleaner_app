<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserLocation extends Model
{
    public function appUsers()
    {
        return $this->belongsTo(AppUser::class);
    }
}

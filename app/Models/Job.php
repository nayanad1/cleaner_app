<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
    protected $fillable = ['request_id', 'cleaner_id'];

    public function appUser()
    {
        return $this->belongsTo(AppUser::class);
    }

    public function cleaner()
    {
        return $this->belongsTo(Cleaner::class);
    }
}

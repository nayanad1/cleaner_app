<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CleanerLocation extends Model
{
    public function userLocation()
    {
        return $this->belongsTo(Cleaner::class);
    }
}

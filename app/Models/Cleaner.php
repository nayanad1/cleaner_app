<?php

namespace App\Models;

use App\Models\CleanerLocation;
use App\Models\Job;
use Illuminate\Database\Eloquent\Model;

class Cleaner extends Model
{
    protected $fillable = ['name', 'email', 'password', 'contact_no', 'token'];

    public function cleanerLocation()
    {
        return $this->hasOne(CleanerLocation::class);
    }

    public function jobs()
    {
        return $this->hasMany(Job::class);
    }
}

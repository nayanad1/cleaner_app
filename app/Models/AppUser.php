<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AppUser extends Model
{
    protected $fillable = ['name', 'email', 'contact_no', 'token', 'password'];

    public function location()
    {
        return $this->hasOne(UserLocation::class);
    }

    public function requests()
    {
        return $this->hasMany(Request::class);
    }

    public function jobs()
    {
        return $this->hasMany(Job::class);
    }
}

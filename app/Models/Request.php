<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Request extends Model
{
    protected $fillable = ['app_user_id', 'longitude', 'latitude'];
    protected $table = 'user_requests';

    public function appUser()
    {
        return $this->belongsTo(AppUser::class);
    }
}

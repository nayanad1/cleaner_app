<?php

namespace App\Http\Controllers;

use App\Models\Admin;
use App\Models\AppUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('login');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data["email"] = $request->email;
        $data["password"] = Hash::make($request->password);

        if (Admin::where('email', $data['email'])->count() > 0) {
            dd("email already available try with another email");
        } else {
            if (Admin::create($data)) {
                return $this->addAdmin();
            } else {
                dd("error");
            }
        }

    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function login(Request $request)
    {
        $userData = Admin::whereEmail($request->input('email'))->first();
        if (Hash::check($request->input('password'), $userData->password)) {
            return $this->home();
        } else {
            dd('Wrong Credentials');
        }
    }

    public function home()
    {
        $userData = AppUser::all();
        return view('home', compact('userData'));
    }

    public function logout()
    {
        return view('login');
    }

    public function addAdmin()
    {
        return view('add_admin');
    }
}

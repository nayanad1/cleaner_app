<?php

namespace App\Http\Controllers;

use App\Models\Cleaner;
use App\Models\Job;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class CleanerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->header('apiKey') === "adc345lktero78xj-2s2#nwusn#") {
            $bodyContent = json_decode($request->getContent(), true);

            if (Cleaner::where('email', $bodyContent['email'])->count() > 0) {
                return response()->json([
                    'status' => false,
                    'message' => 'email already avalable try with another email'
                ]);
            } else {
                $bodyContent["password"] = Hash::make($bodyContent["password"]);
                $data = Cleaner::create($bodyContent);
                if ($data) {
                    return response()->json([
                        'status' => true,
                        'message' => 'created Cleaner successfully'
                    ]);
                } else {
                    return response()->json([
                        'status' => false,
                        'message' => 'something went wrong'
                    ]);
                }
            }
        } else {
            return response()->json([
                'status' => false,
                'message' => 'check your api key'
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //find the given cleaner
        $cleaner = Cleaner::find($id);

        //delete cleaner
        if ($cleaner->delete()) {
            return $this->viewCleaners();
        } else {
            dd("error");
        }
        //
    }

    public function login(Request $request)
    {
        if ($request->header('apiKey') === "adc345lktero78xj-2s2#nwusn#") {
            $bodyContent = json_decode($request->getContent(), true);
            $cleanerData = Cleaner::whereEmail($bodyContent["email"])->first();

            if ($cleanerData){
                if (Hash::check($bodyContent["password"], $cleanerData->password)) {
                    $data = Cleaner::find($cleanerData->id);
                    $data->token = $bodyContent["token"];
                    $data->longitude = $bodyContent["long"];
                    $data->latitude = $bodyContent["lat"];
                    if ($data->save()) {
                        return response()->json([
                            'status' => true,
                            'message' => 'Logged in Successfully'
                        ]);
                    } else {
                        return response()->json([
                            'status' => false,
                            'message' => 'something went wrong try again later'
                        ]);
                    }
                } else {
                    return response()->json([
                        'status' => false,
                        'message' => 'Incorrect Password'
                    ]);
                }
            }else{
                return response()->json([
                    'status' => false,
                    'message' => 'Invalid Email Check and resend the request'
                ]);
            }

        } else {
            return response()->json([
                'status' => false,
                'message' => 'check your api key'
            ]);
        }
    }

    public function logout(Request $request)
    {
        if ($request->header('apiKey') === "adc345lktero78xj-2s2#nwusn#") {
            $bodyContent = json_decode($request->getContent(), true);
            $email = $bodyContent["email"];
            $data = Cleaner::where('email',$email)->firstOrFail();
            if (Hash::check($bodyContent["password"], $data->password)) {
                if ($data->save()) {
                    return response()->json([
                        'status' => true,
                        'message' => 'Log out successfully'
                    ]);
                } else {
                    return response()->json([
                        'status' => false,
                        'message' => 'Something Went Wrong'
                    ]);
                }
            }
        } else {
            return response()->json([
                'status' => false,
                'message' => 'Invalid Token'
            ]);
        }
    }

    public function completeJob(Request $request)
    {
//        dd("ddf");

        if ($request->header('apiKey') === "adc345lktero78xj-2s2#nwusn#") {
            $bodyContent = json_decode($request->getContent(), true);
            $token = $bodyContent["token"];
            $cleanerData = Cleaner::where('token', '=', $token)->firstOrFail();
            $data["request_id"] = $bodyContent["request_id"];
            $data["cleaner_id"] = $cleanerData->id;
            $request = \App\Models\Request::find(1);
            if ($request->status) {
                return response()->json([
                    'status' => false,
                    'message' => 'Clean Job Already Completed'
                ]);
            } else {
                $request->cleaner_id = $cleanerData->id;
                $request->status = 1;
                if ($request->save()) {
                    return response()->json([
                        'status' => true,
                        'message' => 'Clean Job Completed Successfully'
                    ]);
                } else {
                    return response()->json([
                        'status' => false,
                        'message' => 'Something Went Wrong'
                    ]);
                }
            }
        } else {
            return response()->json([
                'status' => false,
                'message' => 'check your api key'
            ]);
        }
    }

    public function viewCleaners()
    {
        $cleanersData = Cleaner::all();
        return view('cleaner', compact('cleanersData'));
    }

    public function viewCleaner($id)
    {
        $cleaner = Cleaner::find($id);
        return view('view_cleaner', compact('cleaner'));
    }

    public function addCleaner()
    {
        return view('add_cleaner');
    }

    public function saveCleaner(Request $request)
    {
        $data["password"] = Hash::make($request->password);
        $data["name"] = $request->name;
        $data["email"] = $request->email;
        $data["contact_no"] = $request->contact_no;
        if (Cleaner::create($data)) {
            return $this->viewCleaners();
        }
    }
}

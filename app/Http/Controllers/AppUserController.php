<?php

namespace App\Http\Controllers;


use App\Http\Classes\FirebaseClass;
use App\Http\Classes\UserHelper;
use App\Models\AppUser;
use App\Models\Cleaner;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;


class AppUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //to create a new user

        //to validate the device/user
        if ($request->header('apiKey') === "adc345lktero78xj-2s2#nwusn#") {

            //assign request data to array
            $bodyContent = json_decode($request->getContent(), true);

            //check email availability in the db
            if (AppUser::where('email', $bodyContent["email"])->count() > 0) {
                return response()->json([
                    'status' => false,
                    'message' => 'email already avalable try with another email'
                ]);
            } else {
                //encrypt password using hash method
                $bodyContent["password"] = Hash::make($bodyContent["password"]);
                //insert data to database using elaquent
                $data = AppUser::create($bodyContent);
                if ($data) {
                    return response()->json([
                        'status' => true,
                        'message' => 'created user successfully'
                    ]);
                } else {
                    return response()->json([
                        'status' => false,
                        'message' => 'something went wrong'
                    ]);
                }
            }
        } else {
            return response()->json([
                'status' => false,
                'message' => 'check your api key'
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function login(Request $request)
    {
        if ($request->header('apiKey') === "adc345lktero78xj-2s2#nwusn#") {
            $bodyContent = json_decode($request->getContent(), true);

            //get the inserted email data from DB
            $userData = AppUser::whereEmail($bodyContent["email"])->first();

            //check the email availability in the DB
            if ($userData){
                if (Hash::check($bodyContent["password"], $userData->password)) {
                    $data = AppUser::find($userData->id);
                    //insert token to the
                    $data->token = $bodyContent["token"];
                    if ($data->save()) {
                        return response()->json([
                            'status' => true,
                            'message' => 'Logged in Successfully',
                            'data' => $bodyContent
                        ]);
                    } else {
                        return response()->json([
                            'status' => false,
                            'message' => 'something went wrong try again later'
                        ]);
                    }
                } else {
                    return response()->json([
                        'status' => false,
                        'message' => 'Incorrect Password'
                    ]);
                }
            }else{
                return response()->json([
                    'status' => false,
                    'message' => 'Invalid Email Check and resend the request'
                ]);
            }
        } else {
            return response()->json([
                'status' => false,
                'message' => 'check your api key'
            ]);
        }
    }

    public function logout(Request $request)
    {
        if ($request->header('apiKey') === "adc345lktero78xj-2s2#nwusn#") {
            $bodyContent = json_decode($request->getContent(), true);
            $email = $bodyContent["email"];

            //check token availability in the DB
            $data = AppUser::where('email',$email)->first();
            if (Hash::check($bodyContent["password"], $data->password)) {
                if ($data->save()) {
                    return response()->json([
                        'status' => true,
                        'message' => 'Log out successfullly'
                    ]);
                } else {
                    return response()->json([
                        'status' => false,
                        'message' => 'Something Went Wrong'
                    ]);
                }
            } else {
                return response()->json([
                    'status' => false,
                    'message' => 'Invalid email'
                ]);
            }

        } else {
            return response()->json([
                'status' => false,
                'message' => 'check your api key'
            ]);
        }
    }

    public function requestCleaner(Request $request)
    {
        if ($request->header('apiKey') === "adc345lktero78xj-2s2#nwusn#") {

            $bodyContent = json_decode($request->getContent(), true);
            $token = $bodyContent["token"];
            $userData = AppUser::where('token', '=', $token)->firstOrFail();
            if ($userData) {
                $bodyContent["app_user_id"] = $userData->id;
                $data["app_user_id"] = $userData->id;
                $data["longitude"] = $bodyContent["longitude"];
                $data["latitude"] = $bodyContent["latitude"];
                $requestData = \App\Models\Request::create($data);
                $requestId = $requestData->id;
                $tokenArr = array();
                $cleaners = Cleaner::all();

                foreach ($cleaners as $cleaner) {
                    $dat[$cleaner->token] = UserHelper::distanceInKilometers($bodyContent["latitude"], $bodyContent["longitude"], $cleaner->latitude, $cleaner->longitude);
                }
                foreach ($dat as $token => $distance) {

                    array_push($tokenArr, $token);
                }
                FirebaseClass::createInstance($tokenArr, $requestId,$data);
                if ($requestData) {
                    return response()->json([
                        'status' => true,
                        'message' => 'Request Created Successfully'
                    ]);
                } else {
                    return response()->json([
                        'status' => false,
                        'message' => 'Cleaning Request Failed'
                    ]);
                }
            } else {
                return response()->json([
                    'status' => false,
                    'message' => 'Invalid User Credentials'
                ]);
            }

        } else {
            return response()->json([
                'status' => false,
                'message' => 'check your api key'
            ]);
        }
    }

    public function viewUser($id)
    {
        $user = AppUser::find($id);
        return view('user', compact('user'));
    }

}

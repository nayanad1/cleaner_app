<?php

namespace App\Http\Classes;

class UserHelper
{
    public static function distanceInKilometers($lat1, $long1, $lat2, $long2)
    {
        $earthRadiusKm = 6371;
        $dlat = UserHelper::degreeToRadians($lat2 - $lat1);
        $dlon = UserHelper::degreeToRadians($long2 - $long1);

        $lat1 = UserHelper::degreeToRadians($lat1);
        $lat2 = UserHelper::degreeToRadians($lat2);

        $valA = sin($dlat / 2) * sin($dlat / 2) + sin($dlon / 2) * sin($dlon / 2) * cos($lat1) * cos($lat2);
        $valc = 2 * atan2(sqrt($valA), sqrt(1 - $valA));
        return $valc * $earthRadiusKm;
    }

    public static function degreeToRadians($degrees)
    {
        return deg2rad($degrees);
    }
}

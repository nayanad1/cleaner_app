<?php

namespace App\Http\Classes;


use Kreait\Firebase;
use Kreait\Firebase\Factory;
use Kreait\Firebase\Messaging\CloudMessage;
use Kreait\Firebase\Messaging\Notification;

class FirebaseClass
{

    public static function createInstance($token,$id,$data)
    {
        $token = $token;
        $path = public_path('json/cleaner-api-25eba-firebase-adminsdk-4bsob-1888b2ce2d.json');
        $factory = (new Factory)->withServiceAccount($path)->withDatabaseUri('https://cleaner-api-25eba.firebaseio.com/');
        $messaging = $factory->createMessaging();

        $dat = array(
            "requestId"=>$id,
            "longitude" => $data["longitude"],
            "latitude" => $data["latitude"]
        );
        $message = CloudMessage::new()->withNotification(Notification::create('Cleaning Request', 'New Cleaning Request Created'));
        $data = $message->withData($dat);
        $messaging->sendMulticast($message, $token,$data);




    }
}
